﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GalytixMarketAnalytics.Startup))]
namespace GalytixMarketAnalytics
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
