﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalytixMarketAnalytics.Common
{
    public static class AppConstants
    {
        public const string CategoriesFilePath = @"\App_Data\PremiumDataFiles\categories.csv";
        public const string CountriesCommercialPremiumFilePath = @"\App_Data\PremiumDataFiles\countries_commercial_premium.csv";
        public const string CountriesInsurancePremiumFilePath = @"\App_Data\PremiumDataFiles\countries_insurance_premium.csv";
        public const string CountriesPenetrationFilePath = @"\App_Data\PremiumDataFiles\countries_penetration.csv";
        public const string CountriesReInsurancePremiumFilePath = @"\App_Data\PremiumDataFiles\countries_reinsurance_premium.csv";
    }
}