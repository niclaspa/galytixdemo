﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GalytixMarketAnalytics.Common
{
    public static class Extension
    {
        public static IEnumerable<string[]> CSVToObject(this string filePath, bool includeHeader = true)
        {
            if (string.IsNullOrWhiteSpace(filePath)) return null;
            var result = (from line in
                              (from line in File.ReadAllText(filePath).Split('\n')
                               select line.Split(',').Select(x => x.Replace("\"", "").Replace("\r", "")).ToArray())
                          select line);
            if (!includeHeader) return result.Skip(1);
            return result;
        }

    }
}