﻿using GalytixMarketAnalytics.Common;
using GalytixMarketAnalytics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalytixMarketAnalytics.BusinessLayer
{
    /// <summary>
    /// Get All the Chart data From this Entity
    /// </summary>
    public class DataManager
    {
        private static readonly string categoriesFullPath;
        private static readonly string countriesInsurancePremiumFullPath;
        private static List<int> years = Enumerable.Range(2006, 9).ToList();
        private static readonly List<CategoriesModel> categoriesData;
        private static readonly List<InsurancePremiumModel> countriesInsurancePremiumData;
        /// <summary>
        /// Create Datasets from files only one time.
        /// </summary>
        static DataManager()
        {
            categoriesFullPath = HttpContext.Current.Server.MapPath(AppConstants.CategoriesFilePath);
            countriesInsurancePremiumFullPath = HttpContext.Current.Server.MapPath(AppConstants.CountriesInsurancePremiumFilePath);
            categoriesData = GetCategoriesDataSet();
            countriesInsurancePremiumData = GetCountriesInsurancePremiumDataSet();
        }
        public static List<TreeMapViewModel> GetTreeMapData(int year)
        {
            return (from cat in categoriesData
                    join insPre in countriesInsurancePremiumData on cat.Id equals insPre.ProductId
                    where insPre.Year == year
                    select new TreeMapViewModel()
                    {
                        Country = insPre.CountryName,
                        Premium = insPre.Premium,
                        Product = cat.Name,
                        Color = cat.Color
                    }).ToList();

        }

        public static List<AreaChartViewModel> GetAreaChartData()
        {
            return (from cat in categoriesData
                    join insPre in countriesInsurancePremiumData on cat.Id equals insPre.ProductId
                    select new AreaChartViewModel()
                    {
                        Country = insPre.CountryName,
                        Premium = insPre.Premium,
                        Product = cat.Name,
                        Color = cat.Color,
                        Year = insPre.Year
                    }).ToList();

        }

        private static List<CategoriesModel> GetCategoriesDataSet()
        {
            var content = categoriesFullPath.CSVToObject();
            var header = content.FirstOrDefault();
            var headerAsList = header.Select(x => x.ToLower()).ToList();
            var colorIndex = headerAsList.IndexOf("color");
            var idIndex = headerAsList.IndexOf("id");
            var activeIndex = headerAsList.IndexOf("active");
            var nameIndex = headerAsList.IndexOf("name");
            var totalIndex = headerAsList.IndexOf("total");
            var dataSet = (from line in content.Skip(1)
                           where line.Length == 5
                           select new CategoriesModel
                           {
                               Id = Convert.ToInt32(line[idIndex]),
                               Color = line[colorIndex] ?? "",
                               Name = line[nameIndex] ?? "",
                               Active = line[activeIndex].ToLowerInvariant() == "true",
                               Total = Convert.ToDecimal(line[totalIndex])
                           }).ToList();
            return dataSet;
        }

        private static List<InsurancePremiumModel> GetCountriesInsurancePremiumDataSet()
        {
            var content = countriesInsurancePremiumFullPath.CSVToObject();
            var header = content.FirstOrDefault();
            var headerAsList = header.Select(x => x.ToLower()).ToList();
            var idIndex = headerAsList.IndexOf("id");
            var nameIndex = headerAsList.IndexOf("name");
            var productIdIndex = headerAsList.IndexOf("product_id");
            var productIndex = headerAsList.IndexOf("product");
            var premiumIndex = headerAsList.IndexOf("premium");
            var dataSet = (from line in content.Skip(1)
                           where line.Length > 5
                           let yearsWiseInsurance = (from l in GetYearWisePremium(line.Skip(5).ToList(), years)//lets split out the data with premium every year basis. 
                                                     select new InsurancePremiumModel
                                                     {
                                                         Id = Convert.ToInt32(line[idIndex]),
                                                         CountryName = line[nameIndex] ?? "",
                                                         ProductName = line[productIndex] ?? "",
                                                         ProductId = Convert.ToInt32(line[productIdIndex]),
                                                         Premium = Convert.ToDecimal(l.Value),
                                                         Year = l.Key
                                                     })
                           select yearsWiseInsurance).SelectMany(x => x).ToList();

            return dataSet;
        }
        private static Dictionary<int, string> GetYearWisePremium(List<string> pre, List<int> years)
        {
            var alreadyConsideredYear = new List<int>();
            var dict = new Dictionary<int, string>();
            if (pre.Count == years.Count)
            {
                foreach (var pr in pre)
                {
                    foreach (var yr in years)
                    {
                        if (!alreadyConsideredYear.Contains(yr))
                        {
                            dict.Add(yr, pr);
                            alreadyConsideredYear.Add(yr);
                            break;
                        }

                    }
                }
            }
            return dict;
        }
    }
}