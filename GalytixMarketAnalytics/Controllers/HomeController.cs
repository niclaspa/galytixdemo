﻿using GalytixMarketAnalytics.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GalytixMarketAnalytics.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Years = Enumerable.Range(2006, 9).OrderByDescending(x => x).Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() });
            return View();
        }

        public ActionResult AreaChart()
        {
            ViewBag.Message = "AreaChart";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public JsonResult RenderTreeMap(int year)
        {
            return Json(DataManager.GetTreeMapData(year));
        }

        [HttpPost]
        public JsonResult RenderAreaChart()
        {
            return Json(DataManager.GetAreaChartData());
        }
    }
}