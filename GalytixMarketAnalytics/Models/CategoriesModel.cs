﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalytixMarketAnalytics.Models
{
    public class CategoriesModel
    {
        public int Id { get; set; }
        public string Color { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public decimal Total { get; set; }
    }
}