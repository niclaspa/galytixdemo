﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalytixMarketAnalytics.Models
{
    public class TreeMapViewModel
    {
        public string Country { get; set; }
        public string Product { get; set; }
        public decimal Premium { get; set; }

        public string Color { get; set; }
    }

    public class AreaChartViewModel
    {
        public string Country { get; set; }
        public string Product { get; set; }
        public decimal Premium { get; set; }

        public int Year { get; set; }

        public string Color { get; set; }
    }
}