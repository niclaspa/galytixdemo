﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalytixMarketAnalytics.Models
{
    public class InsurancePremiumModel
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public string ProductName { get; set; }
        public int ProductId { get; set; }
        public decimal Premium { get; set; }
        public int Year { get; set; }
    }
}